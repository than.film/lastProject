import './App.css';
import {BrowserRouter as Router,Route} from 'react-router-dom'
//---------------------------------------//
  import Navb from './component/navbar'
//------------------------------------//
  import Home from './Pages/home'
  import Login from "./Pages/Login";
function App() {
  return (
    <div className="App">
      <Navb/>
      <Router>
          <Route exact path = '/' component = {Home}/>
          <Route path = '/login' component = {Login}/>
      </Router>
    </div>
  );
}

export default App;
