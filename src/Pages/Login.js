
import React,{useState} from 'react'
import {Form,Button} from 'react-bootstrap'
import axios from 'axios'

// eslint-disable-next-line react-hooks/rules-of-hooks

function Login() {
    const [email, setEmail] = useState("");
    const [password,setPassword] = useState("");

    const handleSubmit =() =>{

        axios.post('http://localhost:8000/login')
        .then((res)=>{
            console.log(res.data);
            console.log("login Sucess");
        })
        .catch(err=>{
            console.log('error');
        })
        
    }
    return (
        <div>
            <Form>
                <Form.Group controlId="formBasicEmail" className = "mx-sm-3" >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" autoComplete="off" 
                       value = {email} onChange = {e => setEmail(e.target.value)} placeholder="Enter email"  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" 
                        autoComplete= "off" value ={password} onChange = {e => setPassword(e.target.value)} />
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <Button variant="primary" type="submit" onClick ={() => handleSubmit()}>
                    Submit
                </Button>
            </Form>
        </div>
    )
}
export default Login
